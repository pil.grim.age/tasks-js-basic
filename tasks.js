// Solutions for homework tasks

// Insert Task 1 solution here

/**
 * @param {string[]} strs
 * @return {string}
 */
var longestCommonPrefix = function(strs) {
    if(strs.length === 1){
        return strs.at(0);
    } 
    else if( strs.length === 0){
        return "";
    }

    let minLength  = Math.min(...strs.map(item => item.length))
    let longComPref = 0;

    loop: for(let col = 0; col < minLength; ++col){
        for(let row = 0; row < strs.length; ++row){
            if(strs.at(0).at(col) != strs.at(row).at(col)){
                break loop; 
            }
        }
        ++longComPref;
    }
    return strs.at(0).slice(0, longComPref);
};


// Insert Task 2 solution here

/**
 * @param {number} n
 * @return {number}
 */
var countPrimes = function(n) {
    if(n < 2) return 0;
    
    let isPrimesArr = new Array(n).fill(true);
    isPrimesArr[0] = isPrimesArr[1] = false;
    
    for(let i = 2; i <= Math.floor(n**0.5); ++i){
        if(isPrimesArr[i]){
            for(let j = i * i; j < n; j += i){
                isPrimesArr[j] = false;  
            } 
        } 
    }
    
    return isPrimesArr.reduce((sum, current) => (current ? ++sum : sum) , 0);
};


// Insert Task 3 solution here

/**
 * @param {string} s
 * @return {number}
 */
var romanToInt = function(s) {
    let roman = {
        I: 1,   IV: 4,
        V: 5,   IX: 9,
        X: 10,  XL: 40,
        L: 50,  XC: 90,
        C: 100, CD: 400,
        D: 500, CM: 900,
        M: 1000, 
    }

    let result = 0;
    let i = 0
    for(; i < s.length-1; i += 2){
        let slice = s.slice(i, i+2);
        result += (slice in roman) ? roman[slice] : roman[s[i--]]
    } 

    return (i === s.length - 1) ? (result + roman[s.at(-1)]) : result;
};

// Insert Task 4 solution here

/**
 * @param {number[]} nums1
 * @param {number} m
 * @param {number[]} nums2
 * @param {number} n
 * @return {void} Do not return anything, modify nums1 in-place instead.
 */
var merge = function(nums1, m, nums2, n) {
    if(n === 0){
        return;
    }
    if(m === 0){
        nums1.splice(0, m+n, ...nums2.slice());
        return;
    }

    let i1 = m - 1;
    let i2 = n - 1;

    for(let j = m + n - 1; j >= 0; --j){
        if(i2 >= 0){
            if(i1 >= 0){
                nums1[j] = nums1[i1] >= nums2[i2] ? nums1[i1--] : nums2[i2--];
            }
            else{
                nums1.splice(0, i2 + 1, ...nums2.slice(0, i2 +1));
                break
            } 
        }
        else break;
    }
};

// Insert Task 5 solution here

/**
 * @param {number[]} nums
 */
var Solution = function(nums) {
    this.original = nums.slice();
}; 

/**
 * @return {number[]}
 */
Solution.prototype.reset = function() {
    return this.original.slice();
};

/**
 * @return {number[]}
 */
Solution.prototype.shuffle = function() {
    let shuffled = this.original.slice();
    for(let i = shuffled.length - 1; i > 0; --i){
        let j = Math.floor(Math.random() * (i + 1));
        [shuffled[i], shuffled[j]] = [shuffled[j], shuffled[i]];
    }
    return shuffled;
};

/** 
 * Your Solution object will be instantiated and called as such:
 * var obj = new Solution(nums)
 * var param_1 = obj.reset()
 * var param_2 = obj.shuffle()
 */